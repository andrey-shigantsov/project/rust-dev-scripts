#!/bin/bash

SCRIPT_PATH="${BASH_SOURCE}"
while [ -L "${SCRIPT_PATH}" ]; do
  SCRIPT_DIR="$(cd -P "$(dirname "${SCRIPT_PATH}")" >/dev/null 2>&1 && pwd)"
  SCRIPT_PATH="$(readlink "${SCRIPT_PATH}")"
  [[ ${SCRIPT_PATH} != /* ]] && SCRIPT_PATH="${SCRIPT_DIR}/${SCRIPT_PATH}"
done
SCRIPT_PATH="$(readlink -f "${SCRIPT_PATH}")"
SCRIPT_DIR="$(cd -P "$(dirname -- "${SCRIPT_PATH}")" >/dev/null 2>&1 && pwd)"

echo $PWD

echo "FORMAT"
cargo fmt -- --check || { echo "FORMAT: fixing..."; cargo fmt; } || { echo "FORMAT: failure"; exit 1; }

echo "CLIPPY"
cargo clippy --workspace --all-targets --all-features -- -D warnings -D rust_2018_idioms \
  || { echo "CLIPPY: failure"; exit 2; } 

echo "METADATA"
VERMISMATCH=rust-basic-lint-version-mistmatch.json
cargo metadata --format-version 1 | jq '.packages | map(.dependencies) | flatten | map(select(.source != null)) | map(select(.source | test("tt-int.net"))) | unique_by(.source) | group_by(.name) | map(select(length > 1)) | .[] | {(.[0].name): [.[] | .source]}' | tee /tmp/$VERMISMATCH
if [ -s /tmp/$VERMISMATCH ]; then echo "METADATA: failure: version mismatch"; exit 3; fi
